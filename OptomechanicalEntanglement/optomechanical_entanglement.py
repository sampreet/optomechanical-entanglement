import math
import sys

import numpy
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from modules.solver import *
from modules.plotter import *

#####################################################################
#      The main GUI of Application inherited from QGridLayout       #
#####################################################################
class AppGUI(QMainWindow):
    # initialize the core components of the UI
    def __init__(self):
        # return the parent to AppGUI class
        super().__init__()
        # set up the application menu and status bar
        self.initMenuBar()
        self.initStatusBar()
        # set up the layout of the main window
        self.initLayout()
        # configure the application GUI
        self.resize(1024, 576)
        rect_frame = self.frameGeometry()
        # get the screen resolution to to locate its center point
        center_point = QDesktopWidget().availableGeometry().center()
        rect_frame.moveCenter(center_point)
        # set the top left of the app window to that of the rectangle
        self.move(rect_frame.topLeft())
        self.setWindowTitle("Optomechanical System")
        self.setWindowIcon(QIcon('web.png'))
        self.show()

    # initilize the layout for the homepage
    def initLayout(self):
        # create a widget to insert the layout into the main window
        widget_home = QWidget()
        # initilize the application layout from the HomeLayout class
        self.layout = HomeLayout()
        self.layout.btn_count.clicked.connect(self.toggleLayout)
        # add the home layout as the central widget
        widget_home.setLayout(self.layout)
        self.setCentralWidget(widget_home)
        self.progress_bar.reset()

    # initialize the menubar containing the new and exit options
    def initMenuBar(self):
        # create a menubar widget and associate a file menu
        menu_bar_main = self.menuBar()
        file_menu_main = menu_bar_main.addMenu('&File')

        # create the new option in the file menu
        action_new = QAction(QIcon('web.png'), '&New', self)
        action_new.setShortcut('Ctrl+N')
        action_new.setStatusTip("Start a new instance of the application")
        # bind the event to the initLayout handler
        action_new.triggered.connect(self.initLayout)
        file_menu_main.addAction(action_new)

        # create the exit option in the file menu
        action_exit = QAction(QIcon('exit.png'), '&Exit', self)
        action_exit.setShortcut('Ctrl+Q')
        action_exit.setStatusTip("Exit the application")
        # bind the event to the closeEvent handler
        action_exit.triggered.connect(self.close)
        file_menu_main.addAction(action_exit)

    # initialize the status bar to display tips and progress
    def initStatusBar(self):
        # create a statusbar widget
        self.statusBar().showMessage("Welcome")
        # append a progress bar to the status bar
        self.progress_bar = QProgressBar()
        self.statusBar().addPermanentWidget(self.progress_bar)
        # status_main.setFont(QFont('SansSerif', 9))
        self.setStatusTip("Ready")

    # reimplement the event handler to modify the action of QCloseEvent
    def closeEvent(self, event):
        # display a dialog box before exiting the application
        response = QMessageBox.question(self, "Exit Application", "Are you sure you want to quit?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        # take action according to the response
        if response == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    # toggle layout on button click in the home page
    def toggleLayout(self):
        # monitor the cavity count and toggle accordingly
        if self.layout.combo.currentText() == '1':
            # single cavity optomechanical system
            widget = QWidget()
            layout = SingleCavityLayout()
            widget.setLayout(layout)
            self.setCentralWidget(widget)
            self.progress_bar.reset()
        elif self.layout.combo.currentText() == '2':
            # single cavity optomechanical system
            widget = QWidget()
            layout = DoubleCavityLayout()
            widget.setLayout(layout)
            self.setCentralWidget(widget)
            self.progress_bar.reset()

#####################################################################
#           Layout of Homepage inherited from QGridLayout           #
#####################################################################
class HomeLayout(QGridLayout):
    # initialize the super class and the call the layout method
    def __init__(self):
        super().__init__()
        self.setSpacing(5)
        self.initLayout()

    # initialize the components of the layout
    def initLayout(self):
        # initialize the labels and entry fields
        self.label_cavity_count = QLabel("Select the Number of Optomechanical Cavities in the System: ")
        self.label_cavity_count.resize(self.label_cavity_count.sizeHint())
        self.combo = QComboBox()
        self.combo.addItems(['1', '2'])
        self.combo.setCurrentIndex(1)
        self.addWidget(self.label_cavity_count, 0, 0)
        self.addWidget(self.combo, 0, 1)

        # create button to execute desired selection
        self.btn_count = QPushButton("Select")
        self.btn_count.setStatusTip("Click the button to select the number of cavities")
        self.btn_count.resize(self.btn_count.sizeHint())
        self.addWidget(self.btn_count, 1, 1)

#####################################################################
#    Layout of Single Cavity System inherited from QGridLayout      #
#####################################################################
class SingleCavityLayout(QGridLayout):
    # initialize the super class and call the layout method
    def __init__(self):
        super().__init__()
        self.x_axis_option = 'd'
        self.setSpacing(5)
        self.initLayout()

    # initialize the layout of the single cavity optomechanical system
    def initLayout(self):
        # initialize the labels and entry fields
        self.l_drive = QLineEdit()
        self.addWidget(QLabel("Wavelenght of Laser Drive (nm): "), 0, 0)
        self.addWidget(self.l_drive, 0, 1)
        self.l_drive.setText('810')

        self.f_mech = QLineEdit()
        self.addWidget(QLabel("Frequency of Mechanical Oscillator (MHz): "), 1, 0)
        self.addWidget(self.f_mech, 1, 1)
        self.f_mech.setText('10')

        self.decay_mech = QLineEdit()
        self.addWidget(QLabel("Decay Rate of Mechanical Oscillator (Hz): "), 2, 0)
        self.addWidget(self.decay_mech, 2, 1)
        self.decay_mech.setText('100')

        self.length = QLineEdit()
        self.addWidget(QLabel("Length of Cavity (mm): "), 3, 0)
        self.addWidget(self.length, 3, 1)
        self.length.setText('1')

        self.P_0 = QLineEdit()
        self.addWidget(QLabel("Power of Laser Drive (mW): "), 4, 0)
        self.addWidget(self.P_0, 4, 1)
        self.P_0.setText('50')

        self.T = QLineEdit()
        self.addWidget(QLabel("Temperature of System (K): "), 5, 0)
        self.addWidget(self.T, 5, 1)
        self.T.setText('0.4')

        self.F_0 = QLineEdit()
        self.addWidget(QLabel("Finesse of Mirror: "), 6, 0)
        self.addWidget(self.F_0, 6, 1)
        self.F_0.setText('16700')

        self.mass = QLineEdit()
        self.addWidget(QLabel("Effective Mass of Mechanical Mode (ng): "), 7, 0)
        self.addWidget(self.mass, 7, 1)
        self.mass.setText('10')

        self.n_x = QLineEdit()
        self.addWidget(QLabel("Precision Per Unit of X-axis Value: "), 8, 0)
        self.addWidget(self.n_x, 8, 1)
        self.n_x.setText('20')

        self.n_x_max = QLineEdit()
        self.addWidget(QLabel("Maximum Value on X-axis: "), 9, 0)
        self.addWidget(self.n_x_max, 9, 1)
        self.n_x_max.setText('5')

        self.n_y = QLineEdit()
        self.addWidget(QLabel("Precision Per Unit of Y-axis Value: "), 10, 0)
        self.addWidget(self.n_y, 10, 1)
        self.n_y.setText('20')

        self.n_y_max = QLineEdit()
        self.addWidget(QLabel("Maximum Value of Y-axis: "), 11, 0)
        self.addWidget(self.n_y_max, 11, 1)
        self.n_y_max.setText('2')

        self.x_axis = QComboBox()
        self.x_axis.addItems(["Effective Detuning (delta/omega_m)", "Normalized Power (P/P_0)", "Absolute Temperature (T)", "Normalized Finesse (F/F_0)"])
        self.addWidget(QLabel("Variable on X-axis: "), 12, 0)
        self.addWidget(self.x_axis, 12, 1)

        self.t_max = QComboBox()
        self.t_max.addItems(['0', '1', '2', '3', '4'])
        self.t_max.setCurrentIndex(4)
        self.addWidget(QLabel("Maximum Index of Routh Hurwitz Polynomials: "), 13, 0)
        self.addWidget(self.t_max, 13, 1)

        # create buttons to execute desired modules
        btn_routh_hurwitz = QPushButton("Routh Hurwitz Criterion Symbolic Polynomials")
        btn_routh_hurwitz.setStatusTip("Click the button to obtain the Routh Hurwitz Criterion")
        btn_routh_hurwitz.resize(btn_routh_hurwitz.sizeHint())
        btn_routh_hurwitz.clicked.connect(self.connect_routh_hurwitz)
        self.addWidget(btn_routh_hurwitz, 14, 1)

        btn_entanglement_2d = QPushButton("2D Numerical Plot of Entanglement")
        btn_entanglement_2d.setStatusTip("Click the button to obtain a 2D Numerical Plot of Entanglement")
        btn_entanglement_2d.resize(btn_entanglement_2d.sizeHint())
        btn_entanglement_2d.clicked.connect(self.connect_entanglement_2d)
        self.addWidget(btn_entanglement_2d, 15, 1)

        btn_entanglement_3d = QPushButton("3D Numerical Plot of Entanglement")
        btn_entanglement_3d.setStatusTip("Click the button to obtain a 3D Numerical Plot of Entanglement")
        btn_entanglement_3d.resize(btn_entanglement_3d.sizeHint())
        btn_entanglement_3d.clicked.connect(self.connect_entanglement_3d)
        self.addWidget(btn_entanglement_3d, 16, 1)

    # call the required functions in the modules
    def connect_routh_hurwitz(self):
        print ("\n*********************************\n*    Routh-Hurwitz Criterion    *\n*********************************")
        gui.statusBar().showMessage("Calculating the Routh-Hurwitz Polynomials...")
        print ("Calculating the Routh-Hurwitz Polynomials...")
        om_system = SingleCavityOMS(float(self.l_drive.text()), float(self.f_mech.text()), float(self.decay_mech.text()), float(self.length.text()), float(self.P_0.text()), float(self.T.text()), int(self.F_0.text()), float(self.mass.text()), int(self.n_x.text()), int(self.n_x_max.text()), self.x_axis_option, int(self.t_max.currentIndex()))
        routh_hurwitz_list = RouthHurwitzCriterion(om_system.s_A, int(self.t_max.currentIndex()))
        routh_hurwitz_list.sym_routh_hurwitz_list()
        gui.statusBar().showMessage("Done.")
        print ("Done.")

    def connect_entanglement_2d(self):
        print ("******************************************************************\n*    Calculation of Entanglement using Logarithmic Negativity    *\n******************************************************************")
        # initialize the x-axis variation type
        legend = ""
        if self.x_axis.currentIndex() == 0:
            self.x_axis_option = 'd'
            legend = "omega_m = 2*pi*" + str(self.f_mech.text()) + "MHz"
            x_label = "Effective Detuning (delta/omega_m)"
            gui.statusBar().showMessage("Calculating the Entanglement vs. effective Detuning values...")
            print ("Calculating the Entanglement vs. effective Detuning values...")
        elif self.x_axis.currentIndex() == 1:
            self.x_axis_option = 'p'
            legend = "P_0 = " + str(self.P_0.text()) + "mW"
            x_label = "Normalized Power (P/P_0)"
            gui.statusBar().showMessage("Calculating the Entanglement vs. normalized Power values...")
            print ("Calculating the Entanglement vs. normalized Power values...")
        elif self.x_axis.currentIndex() == 2:
            self.x_axis_option = 't'
            legend = "T_0 = " + str(self.T.text()) + "K"
            x_label = "Absolute Temperature (T)"
            gui.statusBar().showMessage("Calculating the Entanglement vs. absolute Temperature values...")
            print ("Calculating the Entanglement vs. absolute Temperature values...")
        elif self.x_axis.currentIndex() == 3:
            self.x_axis_option = 'f'
            legend = "F_0 = " + str(self.F_0.text())
            x_label = "Normalized Finesse (F/F_0)"
            gui.statusBar().showMessage("Calculating the Entanglement vs. normalized Finesse values...")
            print ("Calculating the Entanglement vs. normalized Finesse values...")
        y_label = ""
        z_label = "Entanglement (E_n)"
        
        # pass data to function to calculate entanglement using logarithmic negativity
        om_system = SingleCavityOMS(float(self.l_drive.text()), float(self.f_mech.text()), float(self.decay_mech.text()), float(self.length.text()), float(self.P_0.text()), float(self.T.text()), int(self.F_0.text()), float(self.mass.text()), int(self.n_x.text()), int(self.n_x_max.text()), self.x_axis_option, int(self.t_max.currentIndex()))
        [x_data, z_data] = om_system.calcEntanglementLogNeg()
        y_data = numpy.array([])

        # display the data in a 2D plot
        gui.statusBar().showMessage("Plotting the data...")
        print ("Plotting the data...")
        en_plot = EntanglementPlot(x_data, y_data, z_data, x_label, y_label, z_label, legend)
        en_plot.plot_2d()
        gui.statusBar().showMessage("Done.")
        print("Done.")

    def connect_entanglement_3d(self):
        print ("******************************************************************\n*    Calculation of Entanglement using Logarithmic Negativity    *\n******************************************************************")
        # initialize the x-axis variation type
        if self.x_axis.currentIndex() == 0:
            self.x_axis_option = 'd'
            x_label = "Effective Detuning (delta/omega_m)"
            y_label = "Normalized Power (P/P_0)"
            gui.statusBar().showMessage("Calculating the Entanglement vs. effective Detuning vs. normalized Power values...")
            print ("Calculating the Entanglement vs. effective Detuning vs. normalized Power values...")
        elif self.x_axis.currentIndex() == 1:
            self.x_axis_option = 'p'
            x_label = "Normalized Power (P/P_0)"
            y_label = "Normalized Finesse (F/F_0)"
            gui.statusBar().showMessage("Calculating the Entanglement vs. normalized Power vs. normalized Finesse values...")
            print ("Calculating the Entanglement vs. normalized Power vs. normalized Finesse values...")
        elif self.x_axis.currentIndex() == 2:
            self.x_axis_option = 't'
            x_label = "Absolute Temperature (T)"
            y_label = "Normalized Finesse (F/F_0)"
            gui.statusBar().showMessage("Calculating the Entanglement vs. absolute Temperature vs. normalized Finesse values...")
            print ("Calculating the Entanglement vs. absolute Temperature vs. normalized Finesse values...")
        elif self.x_axis.currentIndex() == 3:
            self.x_axis_option = 'd'
            x_label = "Effective Detuning (delta/omega_m)"
            y_label = "Normalized Finesse (F/F_0)"
            gui.statusBar().showMessage("Calculating the Entanglement vs. effective Detuning vs. normalized Finesse values...")
            print ("Calculating the Entanglement vs. effective Detuning vs. normalized Finesse values...")
        z_label = "Entanglement (E_n)"

        # initialize null arrays to append the solution to
        z_data = numpy.array([])
        y_data = numpy.array([])
        
        # iterate over various values of y-axis variation
        for i in range(1, int(self.n_y_max.text())*int(self.n_y.text()) + 1):
            if self.x_axis.currentIndex() == 0:
                finesse = float(self.F_0.text())
                power = float(self.P_0.text())*i/float(self.n_y.text())
                gui.progress_bar.setValue(float(i)*100/float(self.n_y_max.text())/float(self.n_y.text()))
                gui.statusBar().showMessage("Iteration #" + str(i))
            elif self.x_axis.currentIndex() == 1:
                finesse = float(self.F_0.text())*i/float(self.n_y.text())
                power = float(self.P_0.text())
                gui.progress_bar.setValue(float(i)*100/float(self.n_y_max.text())/float(self.n_y.text()))
                gui.statusBar().showMessage("Iteration #" + str(i))
            elif self.x_axis.currentIndex() == 2:
                finesse = float(self.F_0.text())*i/float(self.n_y.text())
                power = float(self.P_0.text())
                gui.progress_bar.setValue(float(i)*100/float(self.n_y_max.text())/float(self.n_y.text()))
                gui.statusBar().showMessage("Iteration #" + str(i))
            if self.x_axis.currentIndex() == 3:
                finesse = float(self.F_0.text())*i/float(self.n_y.text())
                power = float(self.P_0.text())
                gui.progress_bar.setValue(float(i)*100/float(self.n_y_max.text())/float(self.n_y.text()))
                gui.statusBar().showMessage("Iteration #" + str(i))

            # pass data to function to calculate entanglement using logarithmic negativity
            om_system = SingleCavityOMS(float(self.l_drive.text()), float(self.f_mech.text()), float(self.decay_mech.text()), float(self.length.text()), power, float(self.T.text()), finesse, float(self.mass.text()), int(self.n_x.text()), int(self.n_x_max.text()), self.x_axis_option, int(self.t_max.currentIndex()))
            [x_data, z_data_x] = om_system.calcEntanglementLogNeg()
                
            # update the dataset
            y_data = numpy.append(y_data, float(i)/float(int(self.n_y.text())))
            z_data = numpy.append(z_data, z_data_x)
        
        # prepare the vector grid to plot them
        X, Y = numpy.meshgrid(x_data, y_data)
        Z = z_data.reshape(X.shape)

        # display the data obtained in a 3D plot
        gui.statusBar().showMessage("Plotting the data...")
        print ("Plotting the data...")
        en_plot = EntanglementPlot(X, Y, Z, x_label, y_label, z_label)
        en_plot.plot_3d_surf()
        gui.progress_bar.reset()
        gui.statusBar().showMessage("Done.")
        print("Done.")
    
#####################################################################
#    Layout of Double Cavity System inherited from QGridLayout      #
#####################################################################
class DoubleCavityLayout(QGridLayout):
    # initialize the super class and call the layout method
    def __init__(self):
        super().__init__()
        self.x_axis_option = 'd'
        self.y_axis_option = 'j'
        self.setSpacing(5)
        self.initLayout()

    # initialize the layout of the double cavity optomechanical system
    def initLayout(self):
        # initialize the labels and entry fields
        self.l_drive = QLineEdit()
        self.addWidget(QLabel("Wavelenght of Laser Drive (nm): "), 0, 0)
        self.addWidget(self.l_drive, 0, 1)
        self.l_drive.setText('810')

        self.P_0 = QLineEdit()
        self.addWidget(QLabel("Base Power of Laser Drive (mW): "), 1, 0)
        self.addWidget(self.P_0, 1, 1)
        self.P_0.setText('10')

        self.mass = QLineEdit()
        self.addWidget(QLabel("Effective Mass of Mechanical Mode (ng): "), 2, 0)
        self.addWidget(self.mass, 2, 1)
        self.mass.setText('10')

        self.f_mech = QLineEdit()
        self.addWidget(QLabel("Frequency of Mechanical Oscillator (MHz): "), 3, 0)
        self.addWidget(self.f_mech, 3, 1)
        self.f_mech.setText('10')

        self.decay_mech = QLineEdit()
        self.addWidget(QLabel("Decay Rate of Mechanical Oscillator (gamma_m/omega_m): "), 4, 0)
        self.addWidget(self.decay_mech, 4, 1)
        self.decay_mech.setText('0.00001')

        self.norm_k_1 = QLineEdit()
        self.addWidget(QLabel("Normalized Decay Rate of First Cavity (kappa_1/omega_m): "), 5, 0)
        self.addWidget(self.norm_k_1, 5, 1)
        self.norm_k_1.setText('0.89')

        self.norm_k_2 = QLineEdit()
        self.addWidget(QLabel("Normalized Decay Rate of Second Cavity (kappa_2/omega_m): "), 6, 0)
        self.addWidget(self.norm_k_2, 6, 1)
        self.norm_k_2.setText('100')

        self.d_1 = QLineEdit()
        self.addWidget(QLabel("Detuning of First Cavity (delta_1/omega_m): "), 7, 0)
        self.addWidget(self.d_1, 7, 1)
        self.d_1.setText('1.05')

        self.d_2 = QLineEdit()
        self.addWidget(QLabel("Detuning of Second Cavity (delta_2/omega_m): "), 8, 0)
        self.addWidget(self.d_2, 8, 1)
        self.d_2.setText('50')

        self.G_0 = QLineEdit()
        self.addWidget(QLabel("Coupling Strength of Mechanical and Optical Modes (g_0/omega_m): "), 9, 0)
        self.addWidget(self.G_0, 9, 1)
        self.G_0.setText('0.000015')

        self.J_0 = QLineEdit()
        self.addWidget(QLabel("Base Coupling Strength between the Cavities (j/omega_m): "), 10, 0)
        self.addWidget(self.J_0, 10, 1)
        self.J_0.setText('1')

        self.T = QLineEdit()
        self.addWidget(QLabel("Temperature of System (K): "), 11, 0)
        self.addWidget(self.T, 11, 1)
        self.T.setText('0.4')

        self.criteria_opt = QComboBox()
        self.criteria_opt.addItems(['Negativity of Eigenvalues', 
                                    'Routh-Hurwitz 1st Order', 
                                    'Routh-Hurwitz 2nd Order', 
                                    'Routh-Hurwitz 3rd Order', 
                                    'Routh-Hurwitz 4th Order', 
                                    'Routh-Hurwitz 5th Order', 
                                    'Routh-Hurwitz 6th Order'])
        self.criteria_opt.setCurrentIndex(0)
        self.addWidget(QLabel("Select Stability Criteria: "), 12, 0)
        self.addWidget(self.criteria_opt, 12, 1)

        self.x_axis = QComboBox()
        self.x_axis.addItems([  "Effective Detuning (delta/omega_m)", 
                                "Normalized Decay Rate of First Cavity (kappa_1/omega_m)", 
                                "Normalized Decay Rate of Second Cavity (kappa_2/omega_m)", 
                                "Normalized Power (P/P_0)", 
                                "Absolute Temperature (T)", 
                                "Coupling Constant (G/omega_m)", 
                                "Inter-cavity Coupling (J/omega_m)"])
        self.x_axis_option = ['d', 'f', 's', 'p', 't', 'g', 'j']
        self.x_axis.setCurrentIndex(0)
        self.addWidget(QLabel("Variation of x-axis: "), 13, 0)
        self.addWidget(self.x_axis, 13, 1)

        self.y_axis = QComboBox()
        self.y_axis.addItems([  "Normalized Decay Rate of First Cavity (kappa_1/omega_m)", 
                                "Normalized Decay Rate of Second Cavity (kappa_2/omega_m)", 
                                "Normalized Power (P/P_0)", 
                                "Absolute Temperature (T)", 
                                "Coupling Constant (G/omega_m)", 
                                "Inter-cavity Coupling (J/omega_m)"])
        self.y_axis_option = ['f', 's', 'p', 't', 'g', 'j']
        self.y_axis.setCurrentIndex(5)
        self.addWidget(QLabel("Variation of y-axis: "), 14, 0)
        self.addWidget(self.y_axis, 14, 1)

        self.z_axis = QComboBox()
        self.z_axis.addItems([  "Entanglement (E_n)", 
                                "Steady State Value of First Cavity (alpha_1)", 
                                "Steady State Value of Second Cavity (alpha_2)",
                                "Steady State of Position Quadrature (q_s)"])
        self.z_axis_option = ['e', 'a', 'b', 'q']
        self.z_axis.setCurrentIndex(0)
        self.addWidget(QLabel("Variation of z-axis: "), 15, 0)
        self.addWidget(self.z_axis, 15, 1)

        self.ent_opt = QComboBox()
        self.ent_opt.addItems([ "First Cavity mode and Mechanical mode", 
                                "Second Cavity mode and Mechanical mode", 
                                "First Cavity mode and Second Cavity mode"])
        self.ent_option = ['f', 's', 'i']
        self.ent_opt.setCurrentIndex(0)
        self.addWidget(QLabel("Entanglement Mode: "), 16, 0)
        self.addWidget(self.ent_opt, 16, 1)

        self.n_x = QLineEdit()
        self.addWidget(QLabel("Precision of x-axis: "), 17, 0)
        self.addWidget(self.n_x, 17, 1)
        self.n_x.setText('10')

        self.n_x_max = QLineEdit()
        self.addWidget(QLabel("Maximum Value of x-axis: "), 18, 0)
        self.addWidget(self.n_x_max, 18, 1)
        self.n_x_max.setText('5')

        self.n_y = QLineEdit()
        self.addWidget(QLabel("Precision of y-axis: "), 19, 0)
        self.addWidget(self.n_y, 19, 1)
        self.n_y.setText('10')

        self.n_y_max = QLineEdit()
        self.addWidget(QLabel("Maximum Value of y-axis: "), 20, 0)
        self.addWidget(self.n_y_max, 20, 1)
        self.n_y_max.setText('5')

        # create buttons to execute desired modules
        btn_routh_hurwitz = QPushButton("Routh Hurwitz Criterion Symbolic Polynomials")
        btn_routh_hurwitz.setStatusTip("Click the button to obtain the Routh Hurwitz Criterion")
        btn_routh_hurwitz.resize(btn_routh_hurwitz.sizeHint())
        btn_routh_hurwitz.clicked.connect(self.connect_routh_hurwitz)
        self.addWidget(btn_routh_hurwitz, 21, 1)

        btn_2d = QPushButton("2D Numerical Plot")
        btn_2d.setStatusTip("Click the button to obtain a 2D Numerical Plot")
        btn_2d.resize(btn_2d.sizeHint())
        btn_2d.clicked.connect(self.connect_2d)
        self.addWidget(btn_2d, 22, 1)

        btn_3d = QPushButton("3D Numerical Plot")
        btn_3d.setStatusTip("Click the button to obtain a 3D Numerical Plot")
        btn_3d.resize(btn_3d.sizeHint())
        btn_3d.clicked.connect(self.connect_3d)
        self.addWidget(btn_3d, 23, 1)

    # call the required functions in the modules
    def connect_routh_hurwitz(self):
        print ("\n*********************************\n*    Routh-Hurwitz Criterion    *\n*********************************")
        gui.statusBar().showMessage("Calculating the Routh-Hurwitz Polynomials...")
        print ("Calculating the Routh-Hurwitz Polynomials...")
        om_system = DoubleCavityOMS(    
            float(self.l_drive.text()), 
            float(self.P_0.text()), 
            float(self.mass.text()), 
            float(self.f_mech.text()), 
            float(self.decay_mech.text()), 
            float(self.norm_k_1.text()), 
            float(self.norm_k_2.text()), 
            float(self.d_1.text()),
            float(self.d_2.text()),
            float(self.G_0.text()), 
            float(self.J_0.text()), 
            float(self.T.text()))
        routh_hurwitz_list = RouthHurwitzCriterion(om_system.s_A, int(self.criteria_opt.currentIndex()))
        print(routh_hurwitz_list.sym_routh_hurwitz_list())
        gui.statusBar().showMessage("Done.")
        print("Done.")

    def connect_2d(self):
        # initialize the variation variables
        x_label = self.x_axis.currentText()
        y_label = ""
        z_label = self.z_axis.currentText()
        gui.statusBar().showMessage("Calculating the " + z_label + " vs. " + x_label + " values...")
        print ("Calculating the " + z_label + " vs. " + x_label + " values...")
            
        # initialize null arrays to append the solution to
        y_data = numpy.array([])
        # pass data to function to calculate entanglement using logarithmic negativity
        om_system = DoubleCavityOMS(    
            float(self.l_drive.text()), 
            float(self.P_0.text()), 
            float(self.mass.text()), 
            float(self.f_mech.text()), 
            float(self.decay_mech.text()), 
            float(self.norm_k_1.text()), 
            float(self.norm_k_2.text()), 
            float(self.d_1.text()),
            float(self.d_2.text()),
            float(self.G_0.text()), 
            float(self.J_0.text()), 
            float(self.T.text()))
        
        # toggle between entanglement and steady state calculations
        if self.z_axis_option[self.z_axis.currentIndex()] == 'e':
            print ("******************************************************************\n*    Calculation of Entanglement using Logarithmic Negativity    *\n******************************************************************")
            [x_data, z_data, legend] = om_system.calcEntanglementLogNeg(
                self.ent_option[self.ent_opt.currentIndex()],
                self.x_axis_option[self.x_axis.currentIndex()],
                int(self.n_x.text()),
                int(self.n_x_max.text()),
                int(self.criteria_opt.currentIndex()))
            legend = self.ent_opt.currentText()
        else:
            print ("*****************************************************************\n*              Calculation of Steady State Values               *\n*****************************************************************")
            [x_data, z_data, legend] = om_system.calcSteadyState(
                self.ent_option[self.ent_opt.currentIndex()],
                self.x_axis_option[self.x_axis.currentIndex()],
                self.z_axis_option[self.z_axis.currentIndex()],
                int(self.n_x.text()),
                int(self.n_x_max.text()))

        # display the data obtained in a 3D plot
        gui.statusBar().showMessage("Plotting the data...")
        print ("Plotting the data...")
        en_plot = EntanglementPlot(x_data, y_data, z_data, x_label, y_label, z_label, legend)
        en_plot.plot_2d()
        gui.progress_bar.reset()
        gui.statusBar().showMessage("Done.")
        print("Done.")
    
    def connect_3d(self):
        # ignore if the two axes are same
        if self.x_axis.currentText() == self.y_axis.currentText():
            gui.statusBar().showMessage("Same axis selected!")
            print ("Same axis selected!")
        else:
            # initialize the x-axis variation type
            x_label = self.x_axis.currentText()
            y_label = self.y_axis.currentText()
            z_label = self.z_axis.currentText()
            gui.statusBar().showMessage("Calculating the " + z_label + " vs. " + x_label + " vs. " + y_label + " values...")
            print ("Calculating the " + z_label + " vs. " + x_label + " vs. " + y_label + " values...")

            # initialize null arrays to append the solution to
            z_data = numpy.array([])
            y_data = numpy.array([])
            
            norm_1 = float(self.norm_k_1.text())
            norm_2 = float(self.norm_k_2.text())
            power = float(self.P_0.text())
            temp = float(self.T.text())
            g = float(self.G_0.text())
            j = float(self.J_0.text())
            y_axis_opt = self.y_axis_option[self.y_axis.currentIndex()]
            # iterate over various values of y-axis variation
            for i in range(1, int(self.n_y_max.text())*int(self.n_y.text()) + 1):
                gui.progress_bar.setValue(float(i)*100/float(self.n_y_max.text())/float(self.n_y.text()))
                gui.statusBar().showMessage("Iteration #" + str(i))
                # select the x-axis variable
                if y_axis_opt == 'f':
                    norm_1 = float(self.norm_k_1.text())*i/float(self.n_y.text())
                    legend_y = "kappa_1 = " + str(self.norm_k_1.text()) + "omega_m"
                    y_data_temp = norm_1
                elif y_axis_opt == 's':
                    norm_2 = float(self.norm_k_2.text())*i/float(self.n_y.text())
                    legend_y = "kappa_2 = " + str(self.norm_k_2.text()) + "omega_m"
                    y_data_temp = norm_2
                elif y_axis_opt == 'p':
                    power = float(self.P_0.text())*i/float(self.n_y.text())
                    legend_y = "P_0 = " + str(self.P_0.text()) + "W"
                    y_data_temp = power/float(self.P_0.text())
                elif y_axis_opt == 't':
                    temp = float(self.T.text()) + i/float(self.n_y.text())
                    legend_y = "T_0 = " + str(self.T.text()) + "K"
                    y_data_temp = temp
                elif y_axis_opt == 'g':
                    g = float(self.G_0.text())*i/float(self.n_y.text())
                    legend_y = "G_0 = " + str(self.G_0.text()) + "omega_m"
                    y_data_temp = g
                elif y_axis_opt == 'j':
                    j = float(self.J_0.text())*i/float(self.n_y.text())
                    legend_y = "J_0 = " + str(self.J_0.text()) + "omega_m"
                    y_data_temp = j
                    
                om_system = DoubleCavityOMS(    
                    float(self.l_drive.text()), 
                    power, 
                    float(self.mass.text()), 
                    float(self.f_mech.text()), 
                    float(self.decay_mech.text()), 
                    norm_1, 
                    norm_2, 
                    float(self.d_1.text()),
                    float(self.d_2.text()),
                    g, 
                    j, 
                    temp)
                
                # toggle between entanglement and steady state calculations
                if self.z_axis_option[self.z_axis.currentIndex()] == 'e':
                    print ("******************************************************************\n*    Calculation of Entanglement using Logarithmic Negativity    *\n******************************************************************")
                    [x_data, z_data_x, legend_x] = om_system.calcEntanglementLogNeg(
                        self.ent_option[self.ent_opt.currentIndex()],
                        self.x_axis_option[self.x_axis.currentIndex()],
                        int(self.n_x.text()),
                        int(self.n_x_max.text()),
                        int(self.criteria_opt.currentIndex()))
                else:
                    print ("*****************************************************************\n*              Calculation of Steady State Values               *\n*****************************************************************")
                    [x_data, z_data_x, legend_x] = om_system.calcSteadyState(
                        self.ent_option[self.ent_opt.currentIndex()],
                        self.x_axis_option[self.x_axis.currentIndex()],
                        self.z_axis_option[self.z_axis.currentIndex()],
                        int(self.n_x.text()),
                        int(self.n_x_max.text()))
                        
                    # update the dataset
                y_data = numpy.append(y_data, y_data_temp)
                z_data = numpy.append(z_data, z_data_x)
                
            # prepare the vector grid to plot them
            X, Y = numpy.meshgrid(x_data, y_data)
            Z = z_data.reshape(X.shape)

            # display the data obtained in a 3D plot
            gui.statusBar().showMessage("Plotting the data...")
            print ("Plotting the data...")
            en_plot = EntanglementPlot(X, Y, Z, x_label, y_label, z_label, legend_x + "\n" + legend_y)
            en_plot.plot_3d_surf()
            gui.progress_bar.reset()
            gui.statusBar().showMessage("Done.")
            print("Done.")
    

# run the main loop of the application
if __name__ == '__main__':
    # create application object with command line arguments
    app = QApplication(sys.argv)
    gui = AppGUI()
    # initiate the main loop
    sys.exit(app.exec_())
