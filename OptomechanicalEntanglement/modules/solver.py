# dependencies
import math

import numpy
import scipy.constants
import scipy.linalg
from sympy import *


#############################################################################
# # # #                    Routh-Hurwitz Criterion                    # # # #
#############################################################################
class RouthHurwitzCriterion():
    # initialize the class with the required variables
    def __init__(self, A, t):
        self.A = A
        self.max_t = t
        self.poly_vect = numpy.array([])
        # self.poly_list = self.sym_routh_hurwitz_list()
    # helper function for Symbolic Routh-Hurwitz Criterion
    def sym_transformed_det(self, k):
        # initialize a dummy Identity Matrix of dimension k
        T = Matrix(eye(k))
        
        # evaluate each element of the kXk Transformation Matrix
        for i in range(k):
            for j in range(k):
                if (2*(i+1)-(j+1) < 0) or (2*(i+1)-(j+1) > len(self.poly_vect)-1):
                    T[i,j] = 0
                else:
                    T[i,j] = self.poly_vect[2*(i+1)-(j+1)]

        # return the determinant of the Transformation Matrix
        return det(T)

    # function to calculate Symbolic Routh-Hurwitz Criterion
    def sym_routh_hurwitz_list(self):
        # find the determinant of (A - lambda*I) to obtain the eigenvalue polynomial expression in lambda
        s_l = Symbol('lambda')
        eig_eqn = det(self.A - s_l*eye(len(self.A[0,:])))
        # print(eig_eqn)
        # extract the coefficients of lambda from the eigenvalue polynomial equation
        self.poly_vect = Poly(eig_eqn, s_l).coeffs()
        # print(self.poly_vect)

        # create an empty list to store the determinants
        T = numpy.array([])
        # iterate over the coefficients to find the determinants
        for t in range(self.max_t+1):
            # append the determinants to the list
            T = numpy.append(T, self.sym_transformed_det(t))
            print("\nT[", t, "] = ", T[t])

        # return the list to calling loop
        return T
        # A should have real negative eigenvalues to reach steady state at t = infinity
        # for real negative eigenvalues, all the terms in the list T should be positive

    # helper function for Numerical Routh-Hurwitz Criterion
    def num_transformed_det(self, k):
        # if the matrix is a null, return the coefficient of the highest order
        if k == 0:
            return 1
        # initialize a dummy Identity Matrix of dimension k
        T = numpy.eye(k)
        # evaluate each element of the kXk Transformation Matrix
        for i in range(k):
            for j in range(k):
                if (2*(i+1)-(j+1) < 0) or (2*(i+1)-(j+1) > self.poly_vect.shape[0]-1):
                    T[i,j] = 0
                else:
                    T[i,j] = self.poly_vect[2*(i+1)-(j+1)]

        # return the determinant of the Transformation Matrix
        return numpy.linalg.det(T)

    # a helper function to find out the coefficients of a polynomial given its roots
    def num_coeff_finder(self, arr):
        n = arr.shape[0]
        arr_temp = numpy.array([])
        for k in range(n):
            arr_temp = numpy.append(arr_temp, 1)
        res = numpy.array([1])
        for k in range(n):
            sum = 0
            for i in range(n-k):
                sum = sum + arr[i]*arr_temp[i]
                arr_temp[i] = 0
                for j in range(i+1, n-k):
                    arr_temp[i] = arr[j]*arr_temp[j]
            res = numpy.append(res, sum)
        return res

    # function to calculate Numerical Routh-Hurwitz Criterion
    def num_routh_hurwitz_list(self, coeff_arr):
        # # find the eigenvalues and eigenvectors of the matrix 
        # eig_val, eig_vect = scipy.linalg.eig(self.A)
        # # extract the coefficients using the eigenvalues
        # self.poly_vect = self.num_coeff_finder(eig_val.real)

        # import the coefficient array of the eigenvalue equation
        self.poly_vect = coeff_arr
        # create an empty list to store the determinants
        T = numpy.array([])
        # iterate over the coefficients to find the determinants
        for t in range(self.max_t+1):
            # append the determinants to the list
            T = numpy.append(T, self.num_transformed_det(t))
            # print("\nT[", t, "] = ", T[t])

        # return the list to calling loop
        return T
    


#############################################################################
# # # #          Entanglement using Logarithmic Negativity            # # # #
#############################################################################

#############################################################################
#                   OPTOMECHANICAL ENTANGLEMENT BETWEEN                     #
#                          ONE MOVABLE MIRROR AND                           #
#                             ONE CAVITY FIELD                              #
#############################################################################
class SingleCavityOMS():
    # initialize the cavity optomechanical system with the given data
    def __init__(self, l_drive, f_mech, decay_mech, length, power, T, F, mass, n_x, n_x_max, x_axis, t_max):
        # initialize the variables defining the system to the variables of the class
        self.ang_f_dri = 2*math.pi*scipy.constants.c*pow(10, 9)/l_drive
        self.ang_f_mech = 2 * math.pi * pow(10, 6) * f_mech
        self.gamma_mech = 2*math.pi*decay_mech
        self.L = pow(10, -3) * length
        self.P_0 = pow(10, -3) * power
        self.T_0 = T
        self.F_0 = F
        self.m = pow(10, -12) * mass
        self.n_x = n_x
        self.n_x_max = n_x_max
        self.x_axis = x_axis
        self.t_max = t_max
        self.ang_f_cav = self.ang_f_dri + self.ang_f_mech

        # initialize the symbols
        self.s_w_m = Symbol('omega_m')
        self.s_g_m = Symbol('gamma_m')
        self.s_G = Symbol('G')
        self.s_k = Symbol('kappa')
        self.s_d = Symbol('Delta')
        self.s_X_in = Symbol('X_i')
        self.s_Y_in = Symbol('Y_i')
        self.s_e = Symbol('xi')
        self.s_nbar = Symbol('n_t')
        
        # the drift matrix in the rate equation
        self.A = numpy.matrix([[]])
        self.s_A = Matrix([ [   0,              self.s_w_m,     0,              0           ],
                            [   -self.s_w_m,    -self.s_g_m,    self.s_G,       0           ],
                            [   0,              0,              -self.s_k,      self.s_d    ],
                            [   self.s_G,       0,              -self.s_d,      -self.s_k   ]])
        # the correlation matrix of the quadratures
        self.V = numpy.matrix([[]])

    # helper function to check the fulfilment of Routh Hurwitz Criteria
    def check_routh_hurwitz_criteria(self):
        # define the coefficient array of the eigenvalue equation for the single cavity system
        coeff_arr = numpy.array([   1,
                                    self.gamma_mech + 2*self.k,
                                    pow(self.delta, 2) + 2*self.gamma_mech*pow(self.k, 2) + pow(self.k, 2) + pow(self.ang_f_mech, 2),
                                    pow(self.delta, 2)*self.ang_f_mech + self.ang_f_mech*pow(self.k, 2) + 2*self.k*pow(self.ang_f_mech, 2),
                                    pow(self.delta, 2)*pow(self.ang_f_mech, 2) - self.delta*pow(self.G, 2)*self.ang_f_mech + pow(self.k, 2)*pow(self.ang_f_mech, 2) ])
        
        T = RouthHurwitzCriterion(self.A, self.t_max).num_routh_hurwitz_list(coeff_arr)
        for t in range(T.shape[0]):
            if T[t] < 0:
                return 0
        return 1
        
    # helper function to check whether the coupling exceeds threshold of Routh Hurwitz Criteria
    def check_threshold_coupling(self):
        # calculate the threshold from the criteria expression
        self.G_thres = pow((pow(self.delta, 2) + pow(self.k, 2) * self.ang_f_mech / self.delta), 1/2)
        if self.G_thres >= self.G:
            return 1
        return 0

    # helper function to solve the Lyapunov Equation AV + V(A.T) = -D
    def solve_lyapunov(self):
        # python's inbuilt linear eqution solver function
        return scipy.linalg.solve_sylvester(self.A, self.A.T, -self.D)

    # helper function to calculate the enganglement using logarihmic negativity test
    def calc_logarithmic_neg(self):
        # express the matrix V as [ [A, C], [C.T, B] ]
        A = numpy.matrix(self.V[numpy.ix_([x for x in range(int(self.V.shape[0]/2))],[y for y in range(int(self.V.shape[0]/2))])])
        B = numpy.matrix(self.V[numpy.ix_([x for x in range(int(self.V.shape[0]/2), self.V.shape[0])],[y for y in range(int(self.V.shape[0]/2), self.V.shape[0])])])
        C = numpy.matrix(self.V[numpy.ix_([x for x in range(int(self.V.shape[0]/2))],[y for y in range(int(self.V.shape[0]/2), self.V.shape[0])])])

        # calculate the lowest eigenvalue of the correlation matrix
        SigV = numpy.linalg.det(A) + numpy.linalg.det(B) - 2*(numpy.linalg.det(C))
        EtaMinus = pow(2, -1/2)*pow(SigV - pow(pow(SigV,2) - 4*(numpy.linalg.det(self.V)), 1/2), 1/2)
        # return the maximum of the logarithmic negarivity and zero
        return max(0, -1*(math.log(2*EtaMinus)))

    # the driver function to calculate entanglement on the basis of logarithmic negativity
    def calcEntanglementLogNeg(self):
        # initialize arrays to plot the data
        z_data = numpy.array([])
        x_data = numpy.array([])

        self.delta = self.ang_f_mech
        self.P = self.P_0
        self.T = self.T_0
        self.F = self.F_0
        # iterate over different values
        for i in range(1, self.n_x_max*self.n_x + 1):
            # select the x-axis variable
            if self.x_axis == 'd':
                self.delta = float(i) * self.ang_f_mech / float(self.n_x)
            elif self.x_axis == 'p':
                self.P = float(i) * self.P_0 / float(self.n_x)
            elif self.x_axis == 't':
                self.T = self.T_0 + i/self.n_x
            elif self.x_axis == 'f':
                self.F = float(i) * self.F_0 / float(self.n_x)

            # recalculate the dependent variables
            self.k = math.pi * scipy.constants.c / self.L / self.F
            self.G_0 = (self.ang_f_cav / self.L) * pow(scipy.constants.hbar / (self.m * self.ang_f_mech), 1 / 2)
            print(self.gamma_mech/self.ang_f_mech)
            print(self.k/self.ang_f_mech)
            print(self.G_0/self.ang_f_mech)
            self.n_th = 1 / (math.exp((scipy.constants.hbar * self.ang_f_mech) / (scipy.constants.k * self.T)) - 1)
            self.ang_f_dri = self.ang_f_cav - self.delta
            self.E = pow(2 * self.P * self.k / (scipy.constants.hbar * self.ang_f_dri), 1 / 2)
            self.alpha_s = abs(self.E / (complex(self.k, self.delta)))
            self.G = self.G_0 * self.alpha_s * pow(2, 1 / 2)
            # the drift matrix
            self.A = numpy.matrix([ [   0,                  self.ang_f_mech,    0,              0           ],
                                    [   -self.ang_f_mech,   -self.gamma_mech,   self.G,         0           ],
                                    [   0,                  0,                  -self.k,        self.delta  ],
                                    [   self.G,             0,                  -self.delta,    -self.k     ]])
            # the noise correlation matrix
            self.D = numpy.matrix([ [   0,            0,                                0,          0       ],
                                    [   0,            self.gamma_mech*(1 + 2*self.n_th),0,          0       ],
                                    [   0,            0,                                self.k,     0       ],
                                    [   0,            0,                                0,          self.k  ]])

            # if self.check_threshold_coupling() == 1:
            if self.check_routh_hurwitz_criteria() == 1:
                # calculate the correlation matrix
                self.V = self.solve_lyapunov()
                # populate the entanglement values in an array for the plot
                x_data = numpy.append(x_data, float(i)/float(self.n_x))
                z_data = numpy.append(z_data, self.calc_logarithmic_neg())
            else:
                x_data = numpy.append(x_data, float(i)/float(self.n_x))
                z_data = numpy.append(z_data, 0)
                
        # return the values obtained
        return [x_data, z_data]


#############################################################################
#                   OPTOMECHANICAL ENTANGLEMENT BETWEEN                     #
#                          ONE MOVABLE MIRROR AND                           #
#                            TWO CAVITY FIELDS                              #
#############################################################################
class DoubleCavityOMS():
    # initialize the cavity optomechanical system with the given data
    def __init__(self, l_drive, p_0, mass, f_mech, decay_mech, norm_k_1, norm_k_2, d_1, d_2, g_0, j_0, T):
        # initialize the variables of the system
        self.ang_f_dri = 2*math.pi*scipy.constants.c*pow(10, 9)/l_drive
        self.P_0 = pow(10, -3) * p_0
        self.m = pow(10, -12) * mass
        self.ang_f_mech = 2 * math.pi * pow(10, 6) * f_mech
        self.gamma_mech = self.ang_f_mech * decay_mech
        self.k_1_0 = self.ang_f_mech * norm_k_1
        self.k_2_0 = self.ang_f_mech * norm_k_2
        self.delta_1 = d_1
        self.delta_2 = d_2
        self.g_0 = self.ang_f_mech * g_0
        self.J_0 = self.ang_f_mech * j_0
        self.T_0 = T
        # additional dependent variables
        self.ang_f_cav_1 = self.ang_f_dri - self.delta_1
        self.ang_f_cav_2 = self.ang_f_dri - self.delta_2
        self.A = numpy.matrix([[]])
        self.V = numpy.matrix([[]])
        self.V_en = numpy.matrix([[]])

        # initialize the symbols
        self.s_w_m = Symbol('omega_m')
        self.s_w_c2 = Symbol('omega_c2')
        self.s_g_m = Symbol('gamma_m')
        self.s_G = Symbol('G')
        self.s_k_1 = Symbol('kappa_1')
        self.s_k_2 = Symbol('kappa_2')
        self.s_d = Symbol('Delta')
        self.s_d_2 = Symbol('Delta_2')
        self.s_J = Symbol('J')
        self.s_X_in = Symbol('X_i')
        self.s_Y_in = Symbol('Y_i')
        self.s_e = Symbol('xi')
        self.s_nbar = Symbol('n_t')
        # the drift matrix in the rate equation
        self.s_A = Matrix([ [   0,                  self.s_w_m,     0,              0,              0,                  0               ],
                            [   -self.s_w_m,        -self.s_g_m,    self.s_G,       0,              0,                  0               ],
                            [   0,                  0,              -self.s_k_1,    self.s_d,       0,                  -self.s_J       ],
                            [   self.s_G,           0,              -self.s_d,      -self.s_k_1,    -self.s_J,          0               ],
                            [   0,                  0,              0,              -self.s_J,      -self.s_k_2,        self.s_d_2      ],
                            [   0,                  0,              -self.s_J,      0,              -self.s_d_2,        -self.s_k_2     ]])
        
    # helper function to check the fulfilment of Routh Hurwitz Criteria
    def check_routh_hurwitz_criteria(self, t_max):
        # define the coefficient array of the eigenvalue equation for the single cavity system
        coeff_arr = numpy.array([   1,
	                                self.gamma_mech + 2*self.k_1 + 2*self.k_2, 
	                                pow(self.delta, 2) - 2*pow(self.J, 2) + 2*self.gamma_mech*self.k_1 + 2*self.gamma_mech*self.k_2 + pow(self.k_1, 2) + 4*self.k_1*self.k_2 + pow(self.k_2, 2) + pow(self.delta_2, 2) + pow(self.ang_f_mech, 2),
	                                pow(self.delta, 2)*self.gamma_mech + 2*pow(self.delta, 2)*self.k_2 - 2*pow(self.J, 2)*self.gamma_mech - 2*pow(self.J, 2)*self.k_1 - 2*pow(self.J, 2)*self.k_2 + self.gamma_mech*pow(self.k_1, 2) + 4*self.gamma_mech*self.k_1*self.k_2 + self.gamma_mech*pow(self.k_2, 2) + self.gamma_mech*pow(self.delta_2, 2) + 2*pow(self.k_1, 2)*self.k_2 + 2*self.k_1*pow(self.k_2, 2) + 2*self.k_1*pow(self.delta_2, 2) + 2*self.k_1*pow(self.ang_f_mech, 2) + 2*self.k_2*pow(self.ang_f_mech, 2),
	                                2*pow(self.delta, 2)*self.gamma_mech*self.k_2 + pow(self.delta, 2)*pow(self.k_2, 2) + pow(self.delta, 2)*pow(self.delta_2, 2) + pow(self.delta, 2)*pow(self.ang_f_mech, 2) - self.delta*pow(self.G, 2)*self.ang_f_mech - 2*self.delta*pow(self.J, 2)*self.delta_2 + pow(self.J, 4) - 2*pow(self.J, 2)*self.gamma_mech*self.k_1 - 2*pow(self.J, 2)*self.gamma_mech*self.k_2 - 2*pow(self.J, 2)*self.k_1*self.k_2 - 2*pow(self.J, 2)*pow(self.ang_f_mech, 2) + 2*self.gamma_mech*pow(self.k_1, 2)*self.k_2 + 2*self.gamma_mech*self.k_1*pow(self.k_2, 2) + 2*self.gamma_mech*self.k_1*pow(self.delta_2, 2) + pow(self.k_1, 2)*pow(self.k_2, 2) + pow(self.k_1, 2)*pow(self.delta_2, 2) + pow(self.k_1, 2)*pow(self.ang_f_mech, 2) + 4*self.k_1*self.k_2*pow(self.ang_f_mech, 2) + pow(self.k_2, 2)*pow(self.ang_f_mech, 2) + pow(self.delta_2, 2)*pow(self.ang_f_mech, 2), 
	                                pow(self.delta, 2)*self.gamma_mech*pow(self.k_2, 2) + pow(self.delta, 2)*self.gamma_mech*pow(self.delta_2, 2) + 2*pow(self.delta, 2)*self.k_2*pow(self.ang_f_mech, 2) - 2*self.delta*pow(self.G, 2)*self.k_2*self.ang_f_mech - 2*self.delta*pow(self.J, 2)*self.gamma_mech*pow(self.delta_2, 2) + pow(self.J, 4)*self.gamma_mech - 2*pow(self.J, 2)*self.gamma_mech*self.k_1*self.k_2 - 2*pow(self.J, 2)*self.k_1*pow(self.ang_f_mech, 2) - 2*pow(self.J, 2)*self.k_2*pow(self.ang_f_mech, 2) + self.gamma_mech*pow(self.k_1, 2)*pow(self.k_2, 2) + self.gamma_mech*pow(self.k_1, 2)*pow(self.delta_2, 2) + 2*pow(self.k_1, 2)*self.k_2*pow(self.ang_f_mech, 2) + 2*self.k_1*pow(self.k_2, 2)*pow(self.ang_f_mech, 2) + 2*self.k_1*pow(self.delta_2, 2)*pow(self.ang_f_mech, 2), 
	                                pow(self.delta, 2)*pow(self.k_2, 2)*pow(self.ang_f_mech, 2) + pow(self.delta, 2)*pow(self.delta_2, 2)*pow(self.ang_f_mech, 2) - self.delta*pow(self.G, 2)*pow(self.k_2, 2)*self.ang_f_mech - self.delta*pow(self.G, 2)*pow(self.delta_2, 2)*self.ang_f_mech - 2*self.delta*pow(self.J, 2)*self.delta_2*pow(self.ang_f_mech, 2) + pow(self.G, 2)*pow(self.J, 2)*self.delta_2*self.ang_f_mech + pow(self.J, 4)*pow(self.ang_f_mech, 2) - 2*pow(self.J, 2)*self.k_1*self.k_2*pow(self.ang_f_mech, 2) + pow(self.k_1, 2)*pow(self.k_1, 2)*pow(self.ang_f_mech, 2) + pow(self.k_1, 2)*pow(self.delta_2, 2)*pow(self.ang_f_mech, 2)   ])

        T = RouthHurwitzCriterion(self.A, t_max).num_routh_hurwitz_list(coeff_arr)
        for t in range(T.shape[0]):
            if T[t] < 0:
                return 0
        return 1
    
    # helper function to check for negativity of all eigenvalues of the drift matrix
    def check_eigen_neg_criteria(self):
        eig_val, eig_vect = scipy.linalg.eig(self.A)
        for t in range(eig_val.shape[0]):
            if eig_val[t] >= 0:
                return 0
        return 1
    # helper function to solve the Lyapunov Equation AV + V(A.T) = -D
    def solve_lyapunov(self):
        # python's inbuilt linear eqution solver function
        return scipy.linalg.solve_sylvester(self.A, self.A.T, -self.D)

    # helper function to calculate the enganglement using logarihmic negativity test
    def calc_logarithmic_neg(self):
        # express the matrix V as [ [A, C], [C.T, B] ]
        A = numpy.matrix(self.V_en[numpy.ix_([x for x in range(int(self.V_en.shape[0]/2))],[y for y in range(int(self.V_en.shape[0]/2))])])
        B = numpy.matrix(self.V_en[numpy.ix_([x for x in range(int(self.V_en.shape[0]/2), self.V_en.shape[0])],[y for y in range(int(self.V_en.shape[0]/2), self.V_en.shape[0])])])
        C = numpy.matrix(self.V_en[numpy.ix_([x for x in range(int(self.V_en.shape[0]/2))],[y for y in range(int(self.V_en.shape[0]/2), self.V_en.shape[0])])])

        # calculate the lowest eigenvalue of the correlation matrix
        SigV = numpy.linalg.det(A) + numpy.linalg.det(B) - 2*(numpy.linalg.det(C))
        EtaMinus = pow(2, -1/2)*pow(SigV - pow(pow(SigV,2) - 4*(numpy.linalg.det(self.V_en)), 1/2), 1/2)
        # return the maximum of the logarithmic negarivity and zero
        return max(0, -1*(math.log(2*EtaMinus)))

    # helper function to chech stability criteria
    def check_criteria(self, criteria_opt):
        if criteria_opt == 0:
            return self.check_eigen_neg_criteria()
        else:
            return self.check_routh_hurwitz_criteria(criteria_opt)
    # the driver function to calculate entanglement on the basis of logarithmic negativity
    def calcEntanglementLogNeg(self, ent_opt, x_axis_opt, n_x, n_x_max, criteria_opt):
        # initialize arrays to plot the data
        z_data = numpy.array([])
        x_data = numpy.array([])
        legend = ""
        # iterate over different values
        for i in range(1, n_x_max*n_x + 1):
            self.delta = self.ang_f_mech
            self.k_1 = self.k_1_0
            self.k_2 = self.k_2_0
            self.P = self.P_0
            self.T = self.T_0
            self.G_0 = self.g_0
            self.J = self.J_0
            # select the x-axis variable
            if x_axis_opt == 'd':
                self.delta = float(i) * self.ang_f_mech / float(n_x)
                legend = "omega_m = " + str(self.ang_f_mech) + "Hz"
                x_data_temp = self.delta
            elif x_axis_opt == 'f':
                self.k_1 = float(i) * self.k_1_0 / float(n_x)
                legend = "kappa_1 = " + str(self.k_1_0) + "Hz"
                x_data_temp = self.k_1
            elif x_axis_opt == 's':
                self.k_2 = float(i) * self.k_2_0 / float(n_x)
                legend = "kappa_2 = " + str(self.k_2_0) + "Hz"
                x_data_temp = self.k_2
            elif x_axis_opt == 'p':
                self.P = float(i) * self.P_0 / float(n_x)
                legend = "P_0 = " + str(self.P_0) + "W"
                x_data_temp = self.P
            elif x_axis_opt == 't':
                self.T = self.T_0 + i/n_x
                legend = "T_0 = " + str(self.T_0) + "K"
                x_data_temp = self.T
            elif x_axis_opt == 'g':
                self.G_0 = self.g_0 * i/n_x
                legend = "G_0 = " + str(self.g_0) + "Hz"
                x_data_temp = self.G_0
            elif x_axis_opt == 'j':
                self.J = self.J_0 * i/n_x
                legend = "J_0 = " + str(self.J_0) + "Hz"
                x_data_temp = self.J

            # recalculate the dependent variables
            self.n_th = 1 / (math.exp((scipy.constants.hbar * self.ang_f_mech) / (scipy.constants.k * self.T)) - 1)
            self.ang_f_dri = self.ang_f_cav_1 - self.delta
            self.E = pow(2 * self.P * self.k_1 / (scipy.constants.hbar * self.ang_f_dri), 1 / 2)
            self.alpha = abs(self.E * complex(self.k_2, self.delta_2) / (complex(self.k_2, self.delta_2) * complex(self.k_1, self.delta) + pow(self.J, 2)))
            self.beta = abs(self.J * complex(0, -1 * self.E) / (complex(self.k_2, self.delta_2) * complex(self.k_1, self.delta) + pow(self.J, 2)))
            self.G = self.G_0 * self.alpha * pow(2, 1 / 2)
            # the drift matrix
            self.A = numpy.matrix([ [   0,                  self.ang_f_mech,    0,              0,          0,              0               ],
                                    [   -self.ang_f_mech,   -self.gamma_mech,   self.G,         0,          0,              0               ],
                                    [   0,                  0,                  -self.k_1,      self.delta, 0,              -self.J         ],
                                    [   self.G,             0,                  -self.delta,    -self.k_1,  -self.J,        0               ],
                                    [   0,                  0,                  0,              -self.J,    -self.k_2,      self.delta_2    ],
                                    [   0,                  0,                  -self.J,        0,          -self.delta_2,  -self.k_2       ]])
            # the noise correlation matrix
            self.D = numpy.matrix([ [   0,            0,                                0,          0,          0,          0       ],
                                    [   0,            self.gamma_mech*(1 + 2*self.n_th),0,          0,          0,          0       ],
                                    [   0,            0,                                self.k_1,   0,          0,          0       ],
                                    [   0,            0,                                0,          self.k_1,   0,          0       ],
                                    [   0,            0,                                0,          0,          self.k_2,   0       ],
                                    [   0,            0,                                0,          0,          0,          self.k_2]])

            if self.check_criteria(criteria_opt) == 1:
                # calculate the correlation matrix
                self.V = self.solve_lyapunov()
                if ent_opt == 'f':
                    self.V_en = numpy.matrix([  [   self.V[0][0],   self.V[0][1],   self.V[0][2],   self.V[0][3]    ],
                                                [   self.V[1][0],   self.V[1][1],   self.V[1][2],   self.V[1][3]    ],
                                                [   self.V[2][0],   self.V[2][1],   self.V[2][2],   self.V[2][3]    ],
                                                [   self.V[3][0],   self.V[3][1],   self.V[3][2],   self.V[3][3]    ]])
                elif ent_opt == 's':
                    self.V_en = numpy.matrix([  [   self.V[0][0],   self.V[0][1],   self.V[0][4],   self.V[0][5]    ],
                                                [   self.V[1][0],   self.V[1][1],   self.V[1][4],   self.V[1][5]    ],
                                                [   self.V[4][0],   self.V[4][1],   self.V[4][4],   self.V[4][5]    ],
                                                [   self.V[5][0],   self.V[5][1],   self.V[5][4],   self.V[5][5]    ]])
                elif ent_opt == 'i':
                    self.V_en = numpy.matrix([  [   self.V[2][2],   self.V[2][3],   self.V[2][4],   self.V[2][5]    ],
                                                [   self.V[3][2],   self.V[3][3],   self.V[3][4],   self.V[3][5]    ],
                                                [   self.V[4][2],   self.V[4][3],   self.V[4][4],   self.V[4][5]    ],
                                                [   self.V[5][2],   self.V[5][3],   self.V[5][4],   self.V[5][5]    ]])
                # populate the entanglement values in an array for the plot
                z_data = numpy.append(z_data, self.calc_logarithmic_neg())
            else:
                z_data = numpy.append(z_data, 0)
            x_data = numpy.append(x_data, float(i) / float(n_x))

        # return the data to the calling function
        return [x_data, z_data, legend]

    # the driver function to calculate the steady state values
    def calcSteadyState(self, ent_opt, x_axis_opt, z_axis_opt, n_x, n_x_max):

        # initialize arrays to plot the data
        z_data = numpy.array([])
        x_data = numpy.array([])
        legend = ""
        # iterate over different values
        for i in range(1, n_x_max*n_x + 1):
            self.delta = self.ang_f_mech
            self.k_1 = self.k_1_0
            self.k_2 = self.k_2_0
            self.P = self.P_0
            self.T = self.T_0
            self.G_0 = self.g_0
            self.J = self.J_0
            # select the x-axis variable
            if x_axis_opt == 'd':
                self.delta = float(i) * self.ang_f_mech / float(n_x)
                legend = "omega_m = " + str(self.ang_f_mech) + "Hz"
                x_data_temp = float(i) / float(n_x)
            elif x_axis_opt == 'f':
                self.k_1 = float(i) * self.k_1_0 / float(n_x)
                legend = "omega_m = " + str(self.ang_f_mech) + "Hz"
                x_data_temp = self.k_1/self.ang_f_mech
            elif x_axis_opt == 's':
                self.k_2 = float(i) * self.k_2_0 / float(n_x)
                legend = "omega_m = " + str(self.ang_f_mech) + "Hz"
                x_data_temp = self.k_2/self.ang_f_mech
            elif x_axis_opt == 'p':
                self.P = float(i) * self.P_0 / float(n_x)
                legend = "P_0 = " + str(self.P_0) + "W"
                x_data_temp = self.P/self.P_0
            elif x_axis_opt == 't':
                self.T = self.T_0 + i/n_x
                legend = "T_0 = " + str(self.T_0) + "K"
                x_data_temp = self.T
            elif x_axis_opt == 'g':
                self.G_0 = self.g_0 * i/n_x
                legend = "G_0 = " + str(self.g_0) + "Hz"
                x_data_temp = self.G_0/self.ang_f_mech
            elif x_axis_opt == 'j':
                self.J = self.J_0 * i/n_x
                legend = "J_0 = " + str(self.J_0) + "Hz"
                x_data_temp = self.J/self.ang_f_mech

            # recalculate the dependent variables
            self.n_th = 1 / (math.exp((scipy.constants.hbar * self.ang_f_mech) / (scipy.constants.k * self.T)) - 1)
            self.ang_f_dri = self.ang_f_cav_1 - self.delta
            self.E = pow(2 * self.P * self.k_1 / (scipy.constants.hbar * self.ang_f_dri), 1 / 2)
            self.alpha = abs(self.E * complex(self.k_2, self.delta_2) / (complex(self.k_2, self.delta_2) * complex(self.k_1, self.delta) + pow(self.J, 2)))
            
            # populate the arrays with the required data
            x_data = numpy.append(x_data, x_data_temp)
            if z_axis_opt == 'a':
                z_data = numpy.append(z_data, self.alpha)
            elif z_axis_opt == 'b':
                self.beta = abs(self.J * complex(0, -1 * self.E) / (complex(self.k_2, self.delta_2) * complex(self.k_1, self.delta) + pow(self.J, 2)))
                z_data = numpy.append(z_data, self.beta)
            elif z_axis_opt == 'q':
                self.q = self.G_0 * pow(self.alpha, 2) / self.ang_f_mech
                z_data = numpy.append(z_data, self.q)

        # return the data to the calling function
        return [x_data, z_data, legend]