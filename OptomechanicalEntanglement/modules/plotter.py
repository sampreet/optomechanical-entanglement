# dependencies
import math

import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

#############################################################
#           Class to Plot the Entanglement data             #
#############################################################
class EntanglementPlot():
    # initialize the class with the coordinate data
    def __init__(self, x_data, y_data, z_data, x_label, y_label, z_label, legend):
        self.X = x_data
        self.Y = y_data
        self.Z = z_data
        self.x_label = x_label
        self.y_label = y_label
        self.z_label = z_label
        self.legend = legend
        
    ##
    # helper function to plot a 2d graph
    # input:
    #       X: the array of points in the x-axis
    #       Z: the array of values corresponding to each x-axis value
    #       x-axis: the selection of the x_axis for entanglement variation
    # output:
    #       the plot of Z vs. X
    ##
    def plot_2d(self):
        # plot the corresponding points
        plt.plot(self.X, self.Z, label=self.z_label)

        # decide the title depending on data selected
        plt.title("Plot of " + self.z_label + " vs. " + self.x_label)
        plt.xlabel(self.x_label)
        plt.ylabel(self.z_label)
        plt.legend(labels = [self.legend])

        # display the graph
        plt.show()   

    ##
    # helper function to plot a 3D surface
    # input:
    #       X: the 2D array of points with each row along x-axis
    #       Y: the 2D array of points with each row along x-axis
    #       Z: the 2D array of values with each row corresponding to each x-axis row
    #       x-axis: the selection of the x_axis for entanglement variation
    # output:
    #       the plot of Z vs. (X,Y)
    ##
    def plot_3d_surf(self):
        # plot the corresponding points
        fig = plt.figure()
        ax = fig.gca(projection = '3d')
        surf = ax.plot_surface(self.X, self.Y, self.Z, cmap = cm.coolwarm, rstride = 1, cstride = 1, linewidth = 0, antialiased = False)

        # decide the title and labels depending on data selected
        plt.title("Plot of " + self.z_label + " vs. " + self.x_label + " vs. " + self.y_label)
        ax.set_xlabel(self.x_label)
        ax.set_ylabel(self.y_label)
        ax.set_zlabel(self.z_label)
        fig.colorbar(surf, shrink = 0.5, aspect = 5)

        # display the graph
        plt.show()    
