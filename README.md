# Optomechanical Entanglement

![PyPI](https://img.shields.io/pypi/pyversions/Django.svg?style=flat-square)

> An Entanglement Simulation Application for Cavity Optomechanics

The application is written in Python3.x and uses PyQt5 for the GUI.

## Local Development

Install all dependencies by installing the latest version of Anaconda3 from the [Anaconda Downloads page](https://www.continuum.io/downloads).

To start the application, navigate to the directory containing the driver python script and execute the following in the command window:

```
python optomechanical_entanglement.py
```

## Structure

```sh
├── modules/             				# collections of classes and functions
└── optomechanical_entanglement.py      # the driver python script 
```